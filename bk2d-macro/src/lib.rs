#![recursion_limit = "128"]

extern crate proc_macro;

use proc_macro::TokenStream;
use syn::parse_macro_input;

mod action;
mod player;
mod players;
mod public;

#[proc_macro_derive(Action, attributes(action))]
pub fn derive_action(input: TokenStream) -> TokenStream {
    action::derive(parse_macro_input!(input)).into()
}

#[proc_macro_derive(Public, attributes(public_fields))]
pub fn derive_public(input: TokenStream) -> TokenStream {
    public::derive(parse_macro_input!(input)).into()
}

#[proc_macro_derive(Player, attributes(player_fields))]
pub fn derive_player(input: TokenStream) -> TokenStream {
    player::derive(parse_macro_input!(input)).into()
}

#[proc_macro]
pub fn use_default_players(input: TokenStream) -> TokenStream {
    players::use_default(parse_macro_input!(input)).into()
}
