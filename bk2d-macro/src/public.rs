use darling::FromDeriveInput;
use proc_macro2::{Span, TokenStream};
use quote::quote;
use syn::{DeriveInput, Ident};

#[derive(Default, FromDeriveInput)]
#[darling(attributes(public_fields), default)]
struct FieldsAttr {
    round: Option<Ident>,
    id: Option<Ident>,
}

pub fn derive(input: DeriveInput) -> TokenStream {
    let attr = FieldsAttr::from_derive_input(&input).expect("failed to parse attributes");
    let round = attr
        .round
        .unwrap_or_else(|| Ident::new("round", Span::call_site()));
    let id = attr
        .id
        .unwrap_or_else(|| Ident::new("id", Span::call_site()));
    let name = &input.ident;
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    quote! {
        impl#impl_generics ::bk2d::Public for #name#ty_generics #where_clause {
            fn round(&self) -> u32 {
                self.#round
            }

            fn id(&self) -> u64 {
                self.#id
            }
        }
    }
}
