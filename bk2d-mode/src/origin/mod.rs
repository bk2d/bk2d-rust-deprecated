use std::{default::Default, fmt};

use bk2d::{
    point::*,
    use_default_players,
    utils::{Players, SimplePublic},
    Action as ActionTrait, Player as PlayerTrait, BK2D,
};

mod error;
pub use self::error::Error;

pub type Result<T> = std::result::Result<T, Error>;
pub type LengthLimit = std::ops::Range<u32>;
pub type Public = SimplePublic<PublicKind, Point>;

#[derive(ActionTrait, Clone, Debug)]
pub enum Action {
    #[action(stage = 1)]
    Walk(Path),
    #[action(stage = 1)]
    Run(Path),
    #[action(stage = 2)]
    Attack(Path),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum PublicKind {
    Run,
    Attack,
}

pub struct Origin {
    board: Board,
    players: Players<Player>,
    options: Options,
    stage: u32,
    public: Vec<Public>,
    round: u32,
}

impl Origin {
    fn sure_start(&self, msg: &str) {
        self.players.sure_start(msg);
    }

    fn set_stage(&mut self, stage: u32, action: &str) -> Result<()> {
        if self.stage >= stage {
            return Err(Error::CannotDoAction(String::from(action)));
        }
        self.stage = stage;
        Ok(())
    }

    fn check_limit(path: Path, limit: LengthLimit) -> Result<()> {
        let length = path.length();
        if !(limit.start <= length && length < limit.end) {
            return Err(Error::OutOfLimits(length, limit.clone()));
        }
        Ok(())
    }

    fn check_player(&self, p: &Player) -> Result<()> {
        p.check_board(self.board)
    }

    fn move_on(&mut self, path: Path, limit: LengthLimit) -> Result<()> {
        Origin::check_limit(path, limit)?;
        self.players.current_mut().location += path;
        Ok(())
    }

    pub fn walk(&mut self, path: Path) -> Result<()> {
        let limit = self.options.walk_length.clone();
        self.move_on(path, limit)
    }

    pub fn run(&mut self, path: Path) -> Result<()> {
        let limit = self.options.run_length.clone();
        let point = self.players.current_ref().location;

        self.move_on(path, limit)?;
        self.public.push(Public {
            kind: PublicKind::Run,
            data: point,
            round: self.round,
            id: self.public.len() as u64,
        });
        Ok(())
    }

    pub fn attack(&mut self, path: Path) -> Result<()> {
        let limit = self.options.attack_length.clone();
        Origin::check_limit(path, limit)?;
        let location = self.players.current_ref().location + path;
        let mut delete = vec![];
        for (i, item) in self.players.players.iter().enumerate() {
            if &item.name != self.current_player() && item.location == location {
                delete.push(i);
            }
        }
        // delete.sort(); // Sorted
        delete.reverse();
        for i in delete {
            self.players.remove(i);
        }
        self.public.push(Public {
            kind: PublicKind::Attack,
            data: location,
            round: self.round,
            id: self.public.len() as u64,
        });
        Ok(())
    }
}

impl BK2D for Origin {
    use_default_players!();

    type Error = Error;
    type Player = Player;
    type Action = Action;
    type ActionResult = ();
    type Public = Public;
    type Options = Options;

    fn new(board: Board, options: Options) -> Origin {
        Origin {
            board,
            options,
            players: Players::new(),
            public: Vec::new(),
            stage: 0,
            round: 0,
        }
    }

    fn start(&mut self) {
        self.round = 1;
        self.players.start()
    }

    fn action(&mut self, action: Action) -> Result<()> {
        use self::Action::*;
        self.sure_start(&format!("Can't {} before start", action.name()));
        self.set_stage(action.stage(), action.name())?;
        let result = match action {
            Walk(path) => self.walk(path),
            Run(path) => self.run(path),
            Attack(path) => self.attack(path),
        };
        for i in self.players.players.iter() {
            i.check_board(self.board)?;
        }
        result
    }

    fn round(&self) -> u32 {
        self.round
    }

    fn public_actions(&self, round: Option<u32>) -> &[Self::Public] {
        let round = if let Some(round) = round {
            round
        } else {
            return &self.public;
        };
        let mid = self.public.binary_search_by_key(&round, |p| p.round);
        let mid = if let Ok(mid) = mid { mid } else { return &[] };
        let mut left = mid;
        let mut right = mid;
        loop {
            if left == 0 || self.public[left - 1].round != round {
                break;
            } else {
                left -= 1;
            }
        }
        loop {
            if right == self.public.len() || self.public[right].round != round {
                break;
            } else {
                right += 1;
            }
        }
        &self.public[left..right]
    }

    fn public_actions_from(&self, start: u64, end: Option<u64>) -> &[Self::Public] {
        use std::convert::TryInto;
        let startu = start.try_into();
        let startu = if let Ok(x) = startu {
            x
        } else {
            unimplemented!()
        };
        if let Some(end) = end {
            if start >= end {
                return &[];
            }
            &self.public[startu..end as usize]
        } else {
            &self.public[startu..]
        }
    }

    fn win(&self) -> Option<&String> {
        if self.players.players.len() == 1 {
            return Some(self.players.current_ident());
        }
        None
    }
}

#[derive(PlayerTrait, Clone, Debug)]
#[player_fields(ident = "name")]
pub struct Player {
    pub name: String,
    pub location: Point,
}

impl Player {
    pub fn new(name: String, location: Point) -> Player {
        Player { name, location }
    }

    fn check_board(&self, board: Board) -> Result<()> {
        if self.location.outside(board) {
            return Err(Error::OutOfBoard(self.clone(), board));
        }
        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct Options {
    pub walk_length: LengthLimit,
    pub run_length: LengthLimit,
    pub attack_length: LengthLimit,
}

impl Default for Options {
    fn default() -> Options {
        Options {
            walk_length: 1..2,
            run_length: 2..3,
            attack_length: 1..2,
        }
    }
}
