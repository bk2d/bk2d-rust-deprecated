use std::fmt;

use bk2d::point::*;

use super::{LengthLimit, Player};

#[derive(Clone, Debug)]
pub enum Error {
    OutOfBoard(Player, Board),
    OutOfLimits(u32, LengthLimit),
    CannotDoAction(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::OutOfBoard(p, Board { w, h }) => {
                write!(f, "Player {} Out Of Board({}, {})", p, w, h)
            }
            Error::OutOfLimits(l, m) => write!(f, "Move {} but limit {:?}", l, m),
            Error::CannotDoAction(s) => write!(f, "Can't {} at this time", s),
        }
    }
}

impl std::error::Error for Error {}
