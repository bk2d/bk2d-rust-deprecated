use crate::origin::{self, Action::*, Error, Origin};
use bk2d::{
    point::{Board, Path, Point},
    BK2D,
};

#[cfg(test)]
mod action {
    use crate::origin::Action::*;
    use bk2d::{point::Path, Action};

    #[test]
    fn name() {
        let path = Path::new(0, 0);
        assert_eq!(Walk(path).name(), "Walk");
        assert_eq!(Run(path).name(), "Run");
        assert_eq!(Attack(path).name(), "Attack");
    }

    #[test]
    fn stage() {
        let path = Path::new(0, 0);
        assert_eq!(Walk(path).stage(), 1);
        assert_eq!(Run(path).stage(), 1);
        assert_eq!(Attack(path).stage(), 2);
    }
}

fn init() -> Origin {
    let mut game = Origin::new(Default::default(), Default::default());
    game.add_player(origin::Player::new("a".to_string(), Point::new(2, 3)))
        .unwrap();
    game.add_player(origin::Player::new("b".to_string(), Point::new(0, 0)))
        .unwrap();
    game
}

#[test]
fn create_game() {
    Origin::new(Default::default(), Default::default());
}

#[test]
fn add_player() {
    let mut game = Origin::new(Default::default(), Default::default());
    game.add_player(origin::Player::new("a".to_string(), Point::new(2, 3)))
        .expect("Fail to add Player a");
}

#[test]
fn player_out() {
    let mut game = init();
    if let Err(Error::OutOfBoard(
        origin::Player {
            name,
            location: Point { x, y },
        },
        Board { w, h },
    )) = game.add_player(origin::Player::new("c".to_string(), Point::new(10, 0)))
    {
        assert_eq!(name, "c");
        assert_eq!(x, 10);
        assert_eq!(y, 0);
        assert_eq!(w, 5);
        assert_eq!(h, 5);
    } else {
        panic!("Not return Err when player out of board");
    }
}

#[test]
fn walk_too_long() {
    let mut game = init();
    game.start();
    if let Err(Error::OutOfLimits(a, _)) = game.action(Walk(Path { dx: 1, dy: 1 })) {
        assert_eq!(a, 2);
    } else {
        panic!("Not return Err when player walk too long");
    }
}

#[test]
fn walk_too_short() {
    let mut game = init();
    game.start();
    if let Err(Error::OutOfLimits(a, _)) = game.action(Walk(Path { dx: 0, dy: 0 })) {
        assert_eq!(a, 0);
    } else {
        panic!("Not return Err when player walk too short");
    }
}

#[test]
fn walk_out_of_board() {
    let mut game = init();
    game.start();
    game.next_player();
    if let Err(Error::OutOfBoard(
        origin::Player {
            name,
            location: Point { x, y },
        },
        Board { w, h },
    )) = game.action(Walk(Path { dx: 0, dy: -1 }))
    {
        assert_eq!(name, "b");
        assert_eq!(x, 0);
        assert_eq!(y, -1);
        assert_eq!(w, 5);
        assert_eq!(h, 5);
    } else {
        panic!("Not return Err when player walk out of board");
    }
}

// Run work same with Walk, ignore test

#[test]
fn normal_walk() -> origin::Result<()> {
    let mut game = init();
    game.start();
    game.action(Walk(Path { dx: 1, dy: 0 }))
}

#[test]
fn normal_run() -> origin::Result<()> {
    let mut game = init();
    game.start();
    game.action(Run(Path { dx: 1, dy: 1 }))
}

#[test]
fn attack() {
    let mut game = init();
    game.start();
    game.next_player();
    assert!(game.win().is_none());
    game.action(Run(Path { dx: 2, dy: 0 })).unwrap();
    game.next_player();
    game.next_player();
    game.action(Run(Path { dx: 0, dy: 2 })).unwrap();
    game.action(Attack(Path::down()))
        .expect("b attack (2, 3) fail)");
    assert_eq!(game.win().unwrap(), "b");
}
