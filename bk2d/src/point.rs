use std::ops::{Add, AddAssign, Sub, SubAssign};

/// A point
///
/// # Example
///
/// ```
/// let point = bk2d::point::Point::new(1, 2);
///
/// assert_eq!(point.x, 1);
/// assert_eq!(point.y, 2);
/// ```
#[derive(Debug, PartialEq, Eq, Copy, Clone, Default)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Point {
    pub fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    /// Distance to origin
    ///
    /// # Example
    ///
    /// ```
    /// assert_eq!(bk2d::point::Point::new(1, 2).length(), 3);
    /// assert_eq!(bk2d::point::Point::new(5, -3).length(), 8);
    /// ```
    pub fn length(self) -> u32 {
        (self.x.abs() + self.y.abs()) as u32
    }

    /// Point inside board
    ///
    /// # Example
    ///
    /// ```
    /// let board = bk2d::point::Board::new(5, 5);
    ///
    /// assert!(bk2d::point::Point::new(0, 4).inside(board));
    /// assert!(bk2d::point::Point::new(3, 0).inside(board));
    /// assert!(bk2d::point::Point::new(3, 4).inside(board));
    /// ```
    pub fn inside(self, board: Board) -> bool {
        !self.outside(board)
    }

    /// Point outside board
    ///
    /// # Example
    ///
    /// ```
    /// let board = bk2d::point::Board::new(5, 5);
    ///
    /// assert!(bk2d::point::Point::new(5, 4).outside(board));
    /// assert!(bk2d::point::Point::new(0, 5).outside(board));
    /// assert!(bk2d::point::Point::new(7, -1).outside(board));
    /// ```
    pub fn outside(self, board: Board) -> bool {
        self.x < 0 || self.x >= board.w as i32 || self.y < 0 || self.y >= board.h as i32
    }
}

impl Add<Path> for Point {
    type Output = Point;

    /// Point moves on the path
    ///
    /// # Example
    ///
    /// ```
    /// let a = bk2d::point::Point::new(3, 1);
    /// let b = bk2d::point::Path::new(-2, 1);
    /// assert_eq!(a + b, bk2d::point::Point::new(1, 2));
    /// ```
    fn add(self, other: Path) -> Point {
        Point {
            x: self.x + other.dx,
            y: self.y + other.dy,
        }
    }
}

impl AddAssign<Path> for Point {
    /// Point moves on the path
    ///
    /// # Example
    ///
    /// ```
    /// let mut a = bk2d::point::Point::new(3, 1);
    /// a += bk2d::point::Path::new(-2, 1);
    /// assert_eq!(a, bk2d::point::Point::new(1, 2));
    /// ```
    fn add_assign(&mut self, other: Path) {
        *self = *self + other
    }
}

impl Sub<Path> for Point {
    type Output = Point;

    /// Point moves backward on the path
    ///
    /// # Example
    ///
    /// ```
    /// let a = bk2d::point::Point::new(1, 2);
    /// let b = bk2d::point::Path::new(-2, 1);
    /// assert_eq!(a - b, bk2d::point::Point::new(3, 1));
    /// ```
    fn sub(self, other: Path) -> Point {
        Point {
            x: self.x - other.dx,
            y: self.y - other.dy,
        }
    }
}

impl SubAssign<Path> for Point {
    /// Point moves backward on the path
    ///
    /// # Example
    ///
    /// ```
    /// let mut a = bk2d::point::Point::new(1, 2);
    /// a -= bk2d::point::Path::new(-2, 1);
    /// assert_eq!(a, bk2d::point::Point::new(3, 1));
    /// ```
    fn sub_assign(&mut self, other: Path) {
        *self = *self - other
    }
}

impl Sub<Point> for Point {
    type Output = Path;

    /// Vector between two points
    ///
    /// # Example
    ///
    /// ```
    /// let a = bk2d::point::Point::new(1, 2);
    /// let b = bk2d::point::Point::new(3, 1);
    /// let sub = a - b;
    /// assert_eq!(sub, bk2d::point::Path::new(-2, 1));
    /// assert_eq!(sub.length(), 3);
    /// ```
    fn sub(self, other: Point) -> Path {
        Path {
            dx: self.x - other.x,
            dy: self.y - other.y,
        }
    }
}

/// A path, or a vector
///
/// # Example
///
/// ```
/// let path = bk2d::point::Path::new(2, 1);
///
/// assert_eq!(path.dx, 2);
/// assert_eq!(path.dy, 1);
///
/// assert_eq!(path.length(), 3); // 2 + 1
/// ```
///
/// # Merge multiple path
///
/// ```
/// let a = bk2d::point::Path::new(2, 1);
/// let b = bk2d::point::Path::new(-3, 2);
/// let c = bk2d::point::Path::new(1, 0);
/// assert_eq!(bk2d::point::Path::merge(&[a, b, c]), bk2d::point::Path::new(0, 3));
/// assert_eq!(a + b + c, bk2d::point::Path::new(0, 3));
/// ```
///
/// # Common direction
///
/// ```
/// assert_eq!(bk2d::point::Path::up(), bk2d::point::Path::new(0, -1));
/// assert_eq!(bk2d::point::Path::down(), bk2d::point::Path::new(0, 1));
/// assert_eq!(bk2d::point::Path::left(), bk2d::point::Path::new(-1, 0));
/// assert_eq!(bk2d::point::Path::right(), bk2d::point::Path::new(1, 0));
/// ```
#[derive(Debug, PartialEq, Eq, Copy, Clone, Default)]
pub struct Path {
    pub dx: i32,
    pub dy: i32,
}

impl Path {
    pub fn new(dx: i32, dy: i32) -> Path {
        Path { dx, dy }
    }

    pub fn up() -> Path {
        Path::new(0, -1)
    }

    pub fn down() -> Path {
        Path::new(0, 1)
    }

    pub fn left() -> Path {
        Path::new(-1, 0)
    }

    pub fn right() -> Path {
        Path::new(1, 0)
    }

    /// Merge multiple path
    ///
    /// # Example
    ///
    /// ```
    /// let a = bk2d::point::Path::new(2, 1);
    /// let b = bk2d::point::Path::new(-3, 2);
    /// let c = bk2d::point::Path::new(1, 0);
    /// assert_eq!(bk2d::point::Path::merge(&[a, b]), bk2d::point::Path::new(-1, 3));
    /// assert_eq!(bk2d::point::Path::merge(&[a, c]), bk2d::point::Path::new(3, 1));
    /// assert_eq!(bk2d::point::Path::merge(&[b, c]), bk2d::point::Path::new(-2, 2));
    /// assert_eq!(bk2d::point::Path::merge(&[a, b, c]), bk2d::point::Path::new(0, 3));
    /// ```
    pub fn merge(paths: &[Path]) -> Path {
        let mut dx = 0;
        let mut dy = 0;
        for a in paths {
            dx += a.dx;
            dy += a.dy;
        }
        Path { dx, dy }
    }

    /// Distance of path
    ///
    /// # Example
    ///
    /// ```
    /// assert_eq!(bk2d::point::Path::new(1, 2).length(), 3);
    /// assert_eq!(bk2d::point::Path::new(5, -3).length(), 8);
    /// ```
    pub fn length(self) -> u32 {
        (self.dx.abs() + self.dy.abs()) as u32
    }
}

impl Add for Path {
    type Output = Path;

    /// Vector Addition
    ///
    /// # Example
    ///
    /// ```
    /// let a = bk2d::point::Path::new(2, 1);
    /// let b = bk2d::point::Path::new(-3, 2);
    /// let c = bk2d::point::Path::new(1, 0);
    /// assert_eq!(a + b, bk2d::point::Path::new(-1, 3));
    /// assert_eq!(a + c, bk2d::point::Path::new(3, 1));
    /// assert_eq!(b + c, bk2d::point::Path::new(-2, 2));
    /// assert_eq!(a + b + c, bk2d::point::Path::new(0, 3));
    /// ```
    fn add(self, other: Path) -> Path {
        Path {
            dx: self.dx + other.dx,
            dy: self.dy + other.dy,
        }
    }
}

/// A board with width [0, w), height [0, h)
///
/// # Example
///
/// ```
/// let board = bk2d::point::Board::new(5, 6);
///
/// assert_eq!(board.w, 5);
/// assert_eq!(board.h, 6);
///
/// let i = bk2d::point::Point::new(0, 4);
/// assert!(board.inside(i));
///
/// let out = bk2d::point::Point::new(5, 3);
/// assert!(board.outside(out));
/// ```
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub struct Board {
    pub w: u32,
    pub h: u32,
}

impl Board {
    pub fn new(w: u32, h: u32) -> Board {
        Board { w, h }
    }

    /// Point inside board
    ///
    /// # Example
    ///
    /// ```
    /// let board = bk2d::point::Board::new(5, 6);
    ///
    /// assert!(board.inside(bk2d::point::Point::new(0, 4)));
    /// assert!(board.inside(bk2d::point::Point::new(3, 0)));
    /// assert!(board.inside(bk2d::point::Point::new(3, 5)));
    /// ```
    pub fn inside(self, p: Point) -> bool {
        p.inside(self)
    }

    /// Point outside board
    ///
    /// # Example
    ///
    /// ```
    /// let board = bk2d::point::Board::new(5, 6);
    ///
    /// assert!(board.outside(bk2d::point::Point::new(-1, 4)));
    /// assert!(board.outside(bk2d::point::Point::new(3, 6)));
    /// assert!(board.outside(bk2d::point::Point::new(10, -2)));
    /// ```
    pub fn outside(self, p: Point) -> bool {
        p.outside(self)
    }
}

impl Default for Board {
    /// Get standard board
    fn default() -> Board {
        Board { w: 5, h: 5 }
    }
}
