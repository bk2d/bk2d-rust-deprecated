use crate::point::*;

pub trait Action {
    fn name(&self) -> &'static str;

    fn stage(&self) -> u32;
}

pub trait Public {
    fn round(&self) -> u32;

    fn id(&self) -> u64;
}

pub trait BK2D {
    type Error: std::error::Error;
    type Player: Player;
    type Action: Action + Clone;
    type ActionResult;
    type Public: Public + Clone;
    type Options: std::default::Default;

    fn new(board: Board, options: Self::Options) -> Self;

    fn add_player(&mut self, p: Self::Player) -> Result<(), Self::Error>;

    fn current_player(&self) -> &<Self::Player as Player>::Ident;

    fn next_player(&mut self) -> &<Self::Player as Player>::Ident;

    fn start(&mut self);

    fn action(&mut self, action: Self::Action) -> Result<Self::ActionResult, Self::Error>;

    fn is_start(&self) -> bool;

    fn round(&self) -> u32;

    fn public_actions(&self, round: Option<u32>) -> &[Self::Public];

    fn public_actions_from(&self, start: u64, end: Option<u64>) -> &[Self::Public];

    fn win(&self) -> Option<&<Self::Player as Player>::Ident>;
}

pub trait Player {
    type Ident;

    fn ident(&self) -> &Self::Ident;
    fn location(&self) -> &Point;
}
